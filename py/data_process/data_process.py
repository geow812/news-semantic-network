import numpy as np
import pandas as pd

import jieba
import re
import jieba.posseg as pseg
import  jieba.analyse as anls
import heapq
from collections import Counter
from data_connect.database_connect import key_person_list
def divid_dict(total_key_words):
    jieba.load_userdict(list(total_key_words.iloc[:, 0]))
    dict_list = list(total_key_words.iloc[:, 0])
    return dict_list


def f(string):
    p = re.compile('[\u4e00-\u9fa5]')
    temp = re.findall(p, string)
    result = ''.join(temp)
    return result

def extra_same(list1, list2):
    same_elem = [x for x in list1 if x in list2]
    return same_elem


def keywords(df, key_person, stock_concepts):
    df = df.dropna(subset=['CONTEXT'])
    df = df.reset_index().drop('index', axis=1)
    df['cut'] = ''
    df['keywords'] = ''
    df['keyperson']=''
    df['stock_concepts']=''
    # p = re.compile('[\u4e00-\u9fa5]')
    # clean_news = re.findall(p, df.loc[0, "CONTEXT"])
    # clean_news_join = ''.join(clean_news)
    # cut_news = jieba.cut(clean_news_join, cut_all=False)
    # join_cut_news = '/'.join(cut_news)
    # p1 = re.compile("\"(.*?)\"")
    # involed_stock = [re.findall(p1, df.loc[0, 'INVOLVED_STOCK'])]
    # stock_concepts = stock_concepts.fillna('')

    tfidf = anls.extract_tags
    # tempwords = [tfidf(join_cut_news)]
    # match = extra_same(tempwords[0], dict_list)
    # match = list([match])

    for i in range(df.shape[0]):
        cut_temp = f(df.loc[i, "CONTEXT"])
        df.loc[i, "CONTEXT"] = cut_temp
        seg_list = jieba.cut(cut_temp, cut_all=False, HMM=True)

        df.loc[i, 'cut'] = '/'.join(seg_list)

        key_temp = tfidf(df.loc[i, 'cut'], topK=25)
        # tempwords.append(key_temp)
        # keywords = tempwords

        # for j in range(int(len(key_temp))):
        #   for k in range(int(stock_concepts.shape[0])):
        #      if key_temp[j] in stock_concepts['SYNONYMS'][k]:
        #         key_temp[j]=stock_concepts['CONCEPTS'][k]
        #    else:
        #       key_temp[j]=key_temp[j]
        stock_code = df.loc[i, 'STOCK_CODE']
        key_person_for_extra_list, stock_concepts_,stock_concepts_for_extra_list,total_key_words_write_ = key_person_list(stock_code, key_person,
                                                                               stock_concepts)
        stock_concepts_ = stock_concepts_.fillna('')

        dict_list = divid_dict(total_key_words_write_)
        match_temp = extra_same([key_temp][0], dict_list)
        key_person_per=extra_same([key_temp][0], key_person_for_extra_list)
        stock_concepts_per=extra_same([key_temp][0], stock_concepts_for_extra_list)

        for j in range(int(len(match_temp))):
            for k in range(int(stock_concepts_.shape[0])):
                if match_temp[j] in stock_concepts_['SYNONYMS'][k]:
                    match_temp[j] = stock_concepts_['CONCEPTS'][k]

        df.loc[i, 'keywords'] = ','.join(match_temp)
        df.loc[i,'keyperson']=','.join(key_person_per)
        df.loc[i,'stock_concepts']=','.join(stock_concepts_per)

        # match.append(match_temp)
        # match_key = match

        # involed_stock_temp = re.findall(p1, df.loc[i, 'INVOLVED_STOCK'])
        # involed_stock.append(involed_stock_temp)

    return df  # match_key, #involed_stock


def get_match_key(df):
    match_key=[i.split(',') for i in df['keywords'].tolist()]
    involed_stock=[i for i in df['INVOLVED_STOCK'].tolist()]
    for i in range(len(involed_stock)):
        temp=re.findall(re.compile("\[(.*)\]"),df.loc[i,'INVOLVED_STOCK'])[0].split(',')
        involed_stock[i]=[re.findall("\"(.*)\"",i)[0] for i in temp]
    return match_key,involed_stock

def position(list1, list2):
    result = np.zeros(len(list1), dtype=int)
    for i in range(len(list1)):
        try:
            position = list2.index(list1[i])
            result[i] = position
        except:
            continue
    return result


def transferinf(x):
    for i in range(x.shape[0]):
        x[i, np.isnan(x[i, :])] = 0
    return x


def pre(x):
    total_company1 = list()
    for i in range(len(x[2])):
        total_company1 += x[2][i]
    #total_company1=[i for i in total_company1 if i in ['格力电器(000651.SZ)', '美的集团(000333.SZ)']]
    total_company = list(set(total_company1))
    #total_company=[i for i in total_company if i in ['格力电器(000651.SZ)', '美的集团(000333.SZ)']]
    no_rep_a = x[1]
    for i in range(len(x[1])):
        no_rep_a[i] = list(set(x[1][i]))

    total = list()
    for i in range(len(no_rep_a)):
        total += no_rep_a[i]
        total = list(set(total))
    return total_company, total_company1, no_rep_a, total


def initial_E(a, total_company, no_rep_a, total):
    E_company_to_concepts = np.zeros((len(total) + len(total_company), len(total_company) + len(total)))
    for i in range(len(a[1])):
        concepts_posi = position(a[1][i], total) + len(total_company)
        company_posi = position(a[2][i], total_company)
        stockconcepts1 = extra_same(no_rep_a[i], total)
        stockconcepts1_posi = position(stockconcepts1, total)
        for j in range(len(company_posi)):
            E_company_to_concepts[concepts_posi, company_posi[j]] += 1
        for k in range(len(stockconcepts1_posi)):
            tempstockconcepts = stockconcepts1_posi[k]
            E_company_to_concepts[
                (np.delete(stockconcepts1_posi, k) + len(total_company)), len(total_company) + tempstockconcepts] += 1

    E_company_sum = np.sum(E_company_to_concepts, axis=0)
    E_company_to_concepts = transferinf(E_company_to_concepts / E_company_sum)

    for i in range(len(total_company), len(total_company) + len(total)):
        temp = list(E_company_to_concepts[:, i])
        max25 = heapq.nlargest(25, temp)
        max25_posi = position(max25, temp)
        E_company_to_concepts[max25_posi, i] = E_company_to_concepts[max25_posi, i]
    return E_company_to_concepts

def counter_number(total_company1):
    counter = Counter(total_company1)
    counter1 = counter.most_common(len(counter))
    total_company_number = list()
    for i in range(len(counter1)):
        temp = [list(counter1[i])[1]]
        total_company_number += temp

    total_company_name = list()
    for i in range(len(counter1)):
        temp = [list(counter1[i])[0]]
        total_company_name += temp
    total_number = sum(total_company_number)
    total_company_ratio = list(np.array(total_company_number) / total_number)
    return total_company_name, total_number, total_company_ratio



def initial_V(total, total_company, total_company_name, total_company_ratio):
    V_company_to_concepts = np.zeros((len(total) + len(total_company)))
    for i in range(len(total_company_name)):
        posi = position([total_company_name[i]], total_company)
        V_company_to_concepts[posi[0]] = total_company_ratio[i]

    return V_company_to_concepts

