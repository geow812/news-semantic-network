import pandas as pd
import jieba
import re
from config import File_List_Dir
def read_data(file,key_person_root,stock_concepts_root):
    df=pd.read_excel(file)
    key_person = pd.read_csv(str(key_person_root))
    stock_concepts = pd.read_csv(str(stock_concepts_root))
    return df,key_person,stock_concepts

def key_person_list(stock_code,key_person,stock_concepts):

    key_person['SECUCODE'] = key_person['SECUCODE'].map(lambda x: str(x).zfill(6))
    file_list_number=[''.join(re.findall('\d',stock_code))]
    key_person=key_person[key_person['SECUCODE'].isin(file_list_number)]

    key_person_write = key_person[['PERSONNAME']]
    stock_concepts_write = stock_concepts[['CONCEPTS']]
    key_person_list = key_person_write.iloc[:, 0].values.tolist()
    stock_concepts_list = stock_concepts_write.iloc[:, 0].values.tolist()
    total_key_words = list(set(stock_concepts_list + key_person_list))
    total_key_words_write_ = pd.DataFrame(total_key_words)
    return key_person_list,stock_concepts,stock_concepts_list,total_key_words_write_

