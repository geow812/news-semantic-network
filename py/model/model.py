import numpy as np
import pandas as pd
import re
def pagerank (max_iters, epsilon, alpha, beta, V_company_to_concepts, E_company_to_concepts, total_company, total):
    vn_1 = V_company_to_concepts.copy()
    old_En = E_company_to_concepts.copy()
    new_En = E_company_to_concepts.copy()
    num1 = len(total_company)
    num2 = (len(total) + len(total_company))
    for i in range(max_iters):

        vn = ((1 - alpha) * np.dot(new_En, vn_1) + alpha * V_company_to_concepts)
        for j in range(len(total_company)):
            new_En[num1:num2, j] = ((1 - beta) * vn[num1:num2] + beta * E_company_to_concepts[num1:num2, j])
        resid = np.linalg.norm(new_En - old_En, ord=1)
        if resid > epsilon:
            old_En = new_En.copy()
            vn_1 = vn.copy()

        else:
            break
    return vn, new_En


def pandas_output(E_company_to_concepts, total_company, vn, new_En, total):
    re_df = pd.DataFrame(columns=["Company", "Code", "Concepts", "Point_Leverage", "Side_Leverage"])
    no_zero_posi = np.where(E_company_to_concepts[:, range(len(total_company))] != 0)
    pair_posi = sorted(list(map(list, zip(*no_zero_posi))), key=lambda x: x[1], reverse=True)
    for i in range(len(pair_posi)):
        name = ''.join(re.sub(r'\(.*\)', '', total_company[pair_posi[i][1]]))
        stock_code = ''.join(re.findall(r'[(](.*?)[)]', total_company[pair_posi[i][1]]))
        concepts = total[pair_posi[i][0] - len(total_company)]
        point_leverage = vn[pair_posi[i][1]]
        side_leverage = new_En[pair_posi[i][0], pair_posi[i][1]]
        add_data = pd.Series(
            {"Company": name, "Code": stock_code, "Concepts": concepts, "Point_Leverage": point_leverage, "Side_Leverage": side_leverage})
        re_df = re_df.append(add_data, ignore_index=True)

    re_df_concepts = pd.DataFrame(columns=["Concepts1", "Concepts2", "Side_Leverage"])
    no_zero_posi_concepts = np.where(
        E_company_to_concepts[:, range(len(total_company), (len(total_company) + len(total)))] != 0)
    pair_posi_concepts = sorted(list(map(list, zip(*no_zero_posi_concepts))), key=lambda x: x[1], reverse=True)
    for i in range(len(pair_posi_concepts)):
        concepts1 = total[(pair_posi_concepts[i][1] - len(total_company))]
        concepts2 = total[pair_posi_concepts[i][0] - len(total_company)]
        side_leverage1 = new_En[:, range(len(total_company), (len(total_company) + len(total)))][
            pair_posi_concepts[i][0], pair_posi_concepts[i][1]]
        add_data1 = pd.Series({"Concepts1": concepts1, "Concepts2": concepts2, "Side_Leverage": side_leverage1})
        re_df_concepts = re_df_concepts.append(add_data1, ignore_index=True)

    return re_df, re_df_concepts