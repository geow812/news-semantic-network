import os
import logging

# define file path
ROOT_DIR = os.path.abspath('.')
# 统一定义，方便未来可能的改名

OUTPUT_DIR = 'output'
INPUT_DIR='input'

#DEMO_INPUT_CSV_PATH = os.path.join(ROOT_DIR, INPUT_DIR, 'demo.csv')
#def File_List_Dir(stock_list):
#    File_List_Dir = []

#    ROOT_DIR  = os.path.abspath('.')
#    INPUT_DIR = 'input'
#    Direct_dir = ROOT_DIR + '/' + INPUT_DIR + '/'
#    temp_dir = [Direct_dir + stock_list[0] + '.xlsx']
#    File_List_Dir += temp_dir
#    return File_List_Dir
File_List_Dir ="/Users/chenxj00/Documents/长江证券/gitlabpro1/news-semantic-network/000651.SZ.xlsx"
Key_Person_Dir=os.path.join(ROOT_DIR, INPUT_DIR, 'key_person1.csv')
Stock_Concepts_Dir=os.path.join(ROOT_DIR, INPUT_DIR, 'stock_concepts.csv')
OUTPUT_CSV_PATH1 = os.path.join(ROOT_DIR, OUTPUT_DIR, 're_df_compnay_to_concepts_000651.csv')
OUTPUT_CSV_PATH2 =os.path.join(ROOT_DIR, OUTPUT_DIR, 're_df_concepts_to_concepts_000651.csv')
df_output_csv_path=os.path.join(ROOT_DIR, OUTPUT_DIR, 'df.csv')
# define constants
DEMO_NUMBER = 888
DEMO_STR = "This is a demo message"

# define log format
LOG_FILE_NAME = os.path.join(ROOT_DIR, 'log', 'demo_project.log')
LOG_FORMAT = '%(asctime)s %(levelname)s %(funcName)s(%(lineno)d): %(message)s'
loggingConfig = {
    'level': logging.INFO,
    'format': LOG_FORMAT,
    'datefmt': '%Y-%m-%d %H:%M:%S',
    'filename': LOG_FILE_NAME,
    'filemode': 'a'
}
