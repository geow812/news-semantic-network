import logging

import pandas as pd

import re

import jieba
import jieba.posseg as pseg
import jieba.analyse as anls
from collections import Counter
from config import  loggingConfig, LOG_FILE_NAME
from data_connect.database_connect import read_data ,key_person_list
from data_process.data_process import  divid_dict, keywords, pre, initial_E, initial_V, counter_number
from model.model import pagerank,pandas_output
from config import Key_Person_Dir,Stock_Concepts_Dir,OUTPUT_CSV_PATH1,OUTPUT_CSV_PATH2,df_output_csv_path,File_List_Dir
from data_process.data_process import get_match_key
# configure log file
logging.basicConfig(**loggingConfig)
logger = logging.getLogger(LOG_FILE_NAME)




def demo_func():
    # read data from database, 注释写中文也可以
    total_company, total_company1, no_rep_a, total=pre(x=[df_,match_key_, involed_stock_])
    #company_list=['格力电器(000651.SZ)','美的集团(000333.SZ)']
    E_company_to_concepts=initial_E(a=[df_, match_key_, involed_stock_], total_company=total_company, no_rep_a=no_rep_a, total=total)
    total_company_name, total_number, total_company_ratio=counter_number(total_company1=total_company1)
    V_company_to_concepts=initial_V(total=total,  total_company=total_company, total_company_name=total_company_name, total_company_ratio=total_company_ratio)
    vn, new_En=  pagerank(max_iters=1000, epsilon=1e-6, alpha=0.1,beta=0.1, V_company_to_concepts=V_company_to_concepts, E_company_to_concepts=E_company_to_concepts, total_company=total_company, total=total)
    #a = keywords(df=df, dict_list=dict_list)
    #b = pre(x=a)
    #c = initial_E(a=a, total_company=b[0], no_rep_a=b[2], total=b[3])
    #count=counter_number(total_company1=b[1])
    #d = initial_V(total=b[3],  total_company=b[0], total_company_name=count[0], total_company_ratio=count[2])
    #e = pagerank(max_iters=1000, epsilon=1e-6, alpha=0.1,beta=0.1, V_company_to_concepts=d, E_company_to_concepts=c, total_company=b[0], total=b[3])
    #g = pandas_output(E_company_to_concepts=c, total_company=b[0], vn=e[0], new_En=e[1], total=b[3])
    re_df, re_df_concepts=pandas_output(E_company_to_concepts= E_company_to_concepts, total_company=total_company, vn=vn, new_En=new_En, total=total)
    #required_compnay = ['格力电器', '美的集团']
    company_to_concepts=re_df
    concepts_to_concepts=re_df_concepts
    company_to_concepts=company_to_concepts[company_to_concepts['Code'].isin(stock_code)]
    company_to_concepts.to_csv(OUTPUT_CSV_PATH1)
    concepts_to_concepts.to_csv(OUTPUT_CSV_PATH2)
if  __name__  ==  "__main__"  :
    # 调试可以写log，可以看到log/demo_project.log文件被写入
    # 也可以print，但最重要的提示信息，如运行时间、运行结果、错误信息需要写log
    logger.info('run demo main')
    stock_code=['000651.SZ']
    df_=pd.read_csv(df_output_csv_path).fillna('')
    df_stock_code=df_[df_['STOCK_CODE']==stock_code[0]]
    match_key_, involed_stock_ = get_match_key(df=df_stock_code)
    for i in range(len(match_key_)):
        if match_key_[i] == ['']:
            match_key_[i] = []
    demo_func()
