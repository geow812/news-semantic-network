from data_connect.database_connect import read_data,key_person_list
from data_process.data_process import keywords
from config import Key_Person_Dir,Stock_Concepts_Dir,OUTPUT_CSV_PATH1,OUTPUT_CSV_PATH2,df_output_csv_path,File_List_Dir

df, key_person_, stock_concepts_, = read_data(file=File_List_Dir, key_person_root=Key_Person_Dir,
                                                                   stock_concepts_root=Stock_Concepts_Dir)
df_= keywords(df=df, key_person=key_person_, stock_concepts=stock_concepts_)
df_.to_csv(df_output_csv_path)